using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WeaponItem : MonoBehaviour
{
    [SerializeField] private WeaponData weaponData;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private TextMeshPro itemName;
    private void OnValidate()
    {
        if (!spriteRenderer)
            spriteRenderer = GetComponent<SpriteRenderer>();

        if (spriteRenderer && weaponData)
        {
            spriteRenderer.color = weaponData.weaponColor;

            if (itemName)
            {
                itemName.SetText(weaponData.Name);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.TryGetComponent<AttackController>(out var _atctrl))
        {
            OnPlayerCollected(_atctrl);
        }
    }

    private void OnPlayerCollected(AttackController _atctrl)
    {
        _atctrl.ChangeWeapon(weaponData);
        gameObject.SetActive(false);
        if (GameplayManager.Instance.TryGetManager<RespawnManager>(out var _respawnManager))
        {
            _respawnManager.StartRespawn(gameObject);
        }
    }

}
