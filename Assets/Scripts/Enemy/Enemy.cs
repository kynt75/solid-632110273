using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : EnemyBase
{
    [SerializeField] private int score = 1;
    public void OnEnable()
    {
        currentHp = 5;
    }

    // ReSharper disable Unity.PerformanceAnalysis
    protected override void Death(GameObject _source)
    {
        gameObject.SetActive(false);
        
        if (GameplayManager.Instance.TryGetManager<RespawnManager>(out var _respawnManager))
        {
            _respawnManager.StartRespawn(gameObject);
        }
        
        if (_source.TryGetComponent<Player>(out _))
        {
                ScoreManager.Instance.AddScore(score);
            
        }
    }

}
