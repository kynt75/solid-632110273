using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Events;

public class PlayerController : MonoBehaviour
{
    
    
    public void OnMove(InputAction.CallbackContext _context)
    {
        Player.instance.moveInput = _context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext _context)
    {
        if (!Player.instance.isGrounded) return;

        Player.instance.rb.AddForce(Vector2.up * Player.instance.jumpForce, ForceMode2D.Impulse);
        Player.instance.isGrounded = false;
    }

    private readonly float checkGroundRayLenght = 0.6f;

    private void FixedUpdate()
    {
        //UpdateMovement
        Player.instance.rb.velocity = new Vector2(Player.instance.moveInput.x * Player.instance.moveSpeed, Player.instance.rb.velocity.y);

        // Flip the player sprite when changing direction
        if (Player.instance.moveInput.x != 0)
        {
            Player.instance.body.localScale = new Vector3(Mathf.Sign(Player.instance.moveInput.x), 1f, 1f);
            
            //firePoint.rotation = Quaternion.Euler(0, 0, _rotate);
        }

        //CheckGround
        RaycastHit2D _hit = Physics2D.Raycast(transform.position, Vector2.down, checkGroundRayLenght, Player.instance.groundLayer);

        Player.instance.isGrounded = _hit.collider != null;
    }

    private void UpdateMovement()
    {

    }

    private void CheckGround()
    {

    }

    private void OnDrawGizmos()
    {
        Debug.DrawRay(transform.position, Vector3.down * checkGroundRayLenght, Color.green);
    }
}
